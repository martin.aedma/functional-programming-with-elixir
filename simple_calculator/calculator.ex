defmodule Calculator do

    def calc(input) do
        cond do            
            #add
            String.contains?(input, "+") -> helper(input, "+") 
            #subtract
            String.contains?(input, "-") -> helper(input, "-")
            #multiply
            String.contains?(input, "*") -> helper(input, "*")
            #divide
            String.contains?(input, "/") -> helper(input, "/")          
            #fail
            true -> "Input error"
        end   
    end

    def helper(input, mark) do
        x = String.split(input, mark) |> Enum.map( fn num -> Integer.parse(num) end)     
        cond do
            Enum.at(x, 0) == :error -> "Input error"
            Enum.at(x, 1) == :error -> "Input error"
            Enum.at(x, 0) |> elem(1) != "" -> "Input error"
            Enum.at(x, 1) |> elem(1) != "" -> "Input error"       
            mark == "+" -> Math.add(Enum.at(x, 0) |> elem(0), Enum.at(x, 1) |> elem(0))
            mark == "-" -> Math.sub(Enum.at(x, 0) |> elem(0), Enum.at(x, 1) |> elem(0))
            mark == "*" -> Math.mul(Enum.at(x, 0) |> elem(0), Enum.at(x, 1) |> elem(0))
            mark == "/" -> Math.div(Enum.at(x, 0) |> elem(0), Enum.at(x, 1) |> elem(0))            
        end
    end

end
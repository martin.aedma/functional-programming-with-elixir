defmodule Math do

    def add(a,b) do
        c = a+b
        info(a,b,"add", c)
    end
    
    def sub(a,b) do
        c = a-b
        info(a,b,"sub",c)
    end

    def mul(a,b) do
        c = a*b
        info(a,b,"mul",c)
    end

    def div(a,b) do
        c= a/b
        info(a,b,"div",c)
    end

    defp info(a,b,c,d) do
        cond do
            c == "add" -> IO.puts "Adding #{a} and #{b} = #{d}"
            c == "sub" -> IO.puts "Subtracting #{b} from #{a} = #{d}"
            c == "mul" -> IO.puts "Multiplying #{a} and #{b} = #{d}"
            c == "div" -> IO.puts "Dividing #{a} with #{b} = #{d}"
        end
    end

end

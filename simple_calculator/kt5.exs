defmodule Home5 do
    def taskOne() do
        input = IO.gets("Calculator ready, enter task: ")
        corrected = String.trim(input)
        result = Calculator.calc(corrected)
        if result !== "Input error" do
            taskOne()
        else
            IO.puts"Input error, exiting calculator"
        end        
    end
end

Home5.taskOne()
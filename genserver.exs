# API use
# iex.bat -S mix // for my windows system I need to use iex.bat)
# Timer.start_link // start GenServer
### Timer.set_timer(1000, fn() -> IO.puts "message" 
### ...(2)> :cancel end) // set timer in milliseconds, function passed as parameter with :cancel this executes task once after set interval
## Timer.set_timer(1000, fn() -> IO.puts "message"
## ...(3)> :ok end) // set timer and pass function with :ok this executes task continously with set interval until cancelled
# Timer.cancel_timer // cancel timer
# Timer.run_now // this will run last setting again


defmodule Timer do
  @moduledoc false
  use GenServer

  require Logger

  @default_state %{timer: nil, callback: nil, interval: 5000}

  @spec start_link(map(), list()) :: :ignore | {:error, any()} | {:ok, pid()}
  def start_link(_state \\ %{}, _opts \\ []) do
    GenServer.start_link(__MODULE__, @default_state, name: __MODULE__)
  end

  @spec run_now() :: any()
  def run_now() do
    GenServer.call(__MODULE__, :run_now)
  end

  @spec set_timer(integer(), fun()) :: any()
  def set_timer(interval, callback) do
    GenServer.call(__MODULE__, {:set_timer, %{interval: interval, callback: callback}})
  end

  @spec cancel_timer() :: any()
  def cancel_timer() do
    GenServer.call(__MODULE__, :cancel_timer)
  end

  # GenServer callbacks
  @impl true
  @spec init(map()) :: {:ok, map()}
  def init(state) do
    {:ok, state}
  end

  @impl true
  @spec handle_call(atom(), tuple(), map()) :: {:reply, :ok, map()}
  def handle_call({:set_timer, extra_state}, _from, state) do
    {:reply, :ok, state |> Map.merge(extra_state) |> schedule_work()}
  end

  @impl true
  def handle_call(:cancel_timer, _from, state) do
    if state[:timer] do
      Process.cancel_timer(state.timer)
    end

    {:reply, :ok, Map.merge(state, %{timer: nil})}
  end

  def handle_call(:run_now, _from, state) do
    {:reply, :ok, run(state)}
  end

  @impl true
  @spec handle_info(atom(), list()) :: {:noreply, map()}
  def handle_info(:run, state) do
    {:noreply, run(state)}
  end

  def handle_info(message, state) do
    Logger.info("Timer server got an unknown message: #{inspect(message)}")
    {:noreply, state}
  end

  # Private API

  defp run(state) do
    case state.callback.() do
      :ok -> schedule_work(state)
      :cancel -> Map.merge(state, %{timer: nil})
    end
  end

  defp schedule_work(state) do
    Logger.debug("Scheduling the next task in #{state[:interval]} ms")

    if state[:timer] do
      Process.cancel_timer(state.timer)
    end

    timer = Process.send_after(self(), :run, state[:interval])
    Map.merge(state, %{timer: timer})
  end
end

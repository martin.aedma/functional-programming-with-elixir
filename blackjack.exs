defmodule Blackjack do

    def startGame() do
        human = "human"
        dealer = "dealer"

        hValue = twoCard(human)
        dValue = twoCard(dealer)

        parent = self()
        hPID = spawn fn -> humanTurn(hValue, parent) end
        dPID = spawn fn -> dealerTurn(dValue, parent) end
        
        send(hPID, {:start, hValue})

        messages(dPID)
    end

    def messages(dPID) do
        receive do
            {:busted, msg} -> IO.puts(msg)
            {:hold, msg} -> sendToDealer(msg, dPID)
        end
    end

    def sendToDealer(hValue, dPID) do
        send(dPID, {:parent, hValue})
        messages(dPID)
    end

    def oneCard(score, parent) do
        card = Enum.random(1..13)
        num = getValue("human", card, score)
        updated = score + num
        IO.puts"Hit! New score : #{updated}"
        cond do
            updated > 21 -> busted(parent)
            true -> choose(updated, parent)
        end        
    end

    def busted(parent) do
        IO.puts"BUSTED! You lose!"
        endGame(parent)
    end


    def endGame(parent) do
        input = IO.gets("Press 1 for new game : ") |> String.trim()
        cond do 
            input == "1" -> startGame()
            true -> send(parent, {:busted, "Exiting..."})
        end
    end

    def choose(hValue, parent) do
       input = IO.gets("press 1 for Hit OR 2 for Hold : ") |> String.trim()
        cond do 
            input == "1" -> oneCard(hValue, parent)
            input == "2" -> send(parent, {:hold, hValue})
        end       
    end
    

    def dealerTurn(dValue, parent) do
        receive do
            {:parent, msg} -> dealerMove(msg, dValue, parent)
        end
    end

    def dealerMove(hValue, dValue, parent) do
        cond do
            (hValue <= dValue) and (dValue < 22) -> dealerWins(dValue,parent)
            (hValue > dValue) and (hValue < 22) -> dealerCard(hValue, dValue, parent)
            dValue > 21 -> dealerBusted(parent)
        end
    end

    def dealerWins(dValue,parent) do
        IO.puts"Dealer Wins with #{dValue}!"
        endGame(parent)
    end

    def dealerCard(hValue, dValue, parent) do        
        card = Enum.random(1..13)
        num = getValue("dealer", card, dValue)
        updated = dValue + num
        IO.puts"Dealer Hits! Now has : #{updated}"
        dealerMove(hValue, updated, parent)
    end

    def dealerBusted(parent) do
        IO.puts"Dealer BUSTED! You win!" 
        endGame(parent)
    end

    def humanTurn(hValue, parent) do
        receive do
            {:start, msg} -> choose(hValue, parent)
        end
    end

    def twoCard(player) do
        one = Enum.random(1..13)
        two = Enum.random(1..13)
        value = getValue(player, one, 0) + getValue(player, two, 0)
        printValue(player, value)
        value
    end

    def getValue(player, cardNum, value) do
        cond do
            (cardNum == 1) and (player == "human") -> aceHuman()
            (cardNum == 1) and (player == "dealer") -> aceDealer(value)
            cardNum < 10 -> cardNum
            cardNum > 9 -> 10
        end
    end

    def aceHuman() do
        value = IO.gets("You got Ace -> press 1 for value 1 OR 2 for value 11 : ") |> String.trim()
        cond do
            value == "1" -> 1
            value == "2" -> 11
        end
    end

    def aceDealer(value) do
        IO.puts"Dealer ACE"
        cond do
            (value + 11) < 22 -> 11
            true -> 1
        end
    end

    def printValue(player, num) do
        cond do
            player == "human" -> IO.puts"You have : #{num}"
            player == "dealer" -> IO.puts"Dealer has : #{num}"
        end
    end

end

Blackjack.startGame

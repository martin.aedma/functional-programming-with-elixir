defmodule Employee do
    defstruct [:fname, :lname, :id, salary: 0, job: "none"]


    def start() do
    list = []
    IO.puts "Program operational!"
    IO.puts "1. check employee list"
    IO.puts "2. add new employee"
    IO.puts "3. promote employee"
    IO.puts "4. demote employee"
    programStart(list)
    IO.puts "Input error, program terminated"
    end
    

    def programStart(list) do
        input = IO.gets("Enter option number : ")
        cInput = String.trim(input)

        cond do
            cInput == "1" -> displayEmployees(list)
            cInput == "2" -> addEmployee(list)
            cInput == "3" -> promote(list)
            cInput == "4" -> demote(list)
            true -> "input error"
        end
    end


    def addEmployee(list) do
        IO.puts "Adding new employee..."
        fnam = IO.gets ("First name: ")
        cFname = String.trim(fnam)
        lnam = IO.gets("Last name: ") 
        cLname = String.trim(lnam)

        if (List.first(list) != nil) do
            idNum = List.first(list).id + 1
            emp = %Employee{fname: cFname, lname: cLname, id: idNum}
            list = [emp | list]
            IO.puts "Employee added!"
            programStart(list)
        else        
            emp = %Employee{fname: cFname, lname: cLname, id: 1}
            list = [emp | list]
            IO.puts "Employee added!"
            programStart(list)
        end       
    end

    
    def displayEmployees(list) do
        x = List.first(list)
        if x != nil do
            list
            |> Enum.each(fn emp -> IO.puts("#{emp.fname} #{emp.lname} #{emp.id} #{emp.salary} #{emp.job}") end)
        else
            IO.puts "Currently 0 employees, add some!"
        end
        programStart(list)
    end

    def promote(list) do
        emp_id =
        "Enter employee ID to promote: "
        |> IO.gets()
        |> String.trim()
        |> String.to_integer()
        
        getEmp = list 
        |> Enum.filter(fn x -> x.id == emp_id end) 
        |> List.first()

        if getEmp != nil do
            cond do
                getEmp.job == "none" -> list |> Enum.map(fn x -> if x.id == emp_id do %{x | job: "coder", salary: 2000} end end) |> programStart()
                getEmp.job == "coder" -> list |> Enum.map(fn x -> if x.id == emp_id do %{x | job: "designer", salary: 4000} end end) |> programStart()
                getEmp.job == "designer" -> list |> Enum.map(fn x -> if x.id == emp_id do %{x | job: "manager", salary: 6000} end end) |> programStart()
                getEmp.job == "manager" -> list |> Enum.map(fn x -> if x.id == emp_id do %{x | job: "ceo", salary: 8000} end end) |> programStart()
                getEmp.job == "ceo" -> list |> programStart()
            end
        else
            IO.puts "no employee with such id"
            list |> programStart()
        end
    end


    def demote(list) do
        emp_id =
        "Enter employee ID to demote: "
        |> IO.gets()
        |> String.trim()
        |> String.to_integer()
        
        getEmp = list 
        |> Enum.filter(fn x -> x.id == emp_id end) 
        |> List.first()

        if getEmp != nil do
            cond do
                getEmp.job == "none" -> list |> programStart()
                getEmp.job == "coder" -> list |> Enum.map(fn x -> if x.id == emp_id do %{x | job: "none", salary: 0} end end) |> programStart()
                getEmp.job == "designer" -> list |> Enum.map(fn x -> if x.id == emp_id do %{x | job: "coder", salary: 2000} end end) |> programStart()
                getEmp.job == "manager" -> list |> Enum.map(fn x -> if x.id == emp_id do %{x | job: "designer", salary: 4000} end end) |> programStart()
                getEmp.job == "ceo" -> list |> Enum.map(fn x -> if x.id == emp_id do %{x | job: "manager", salary: 6000} end end) |> programStart()
            end
        else
            IO.puts "no employee with such id"
            list |> programStart()
        end
    end


end

Employee.start()

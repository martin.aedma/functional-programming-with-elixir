defmodule Home4 do

    def taskOne() do
        colors = [{:black, "#000000"}, {:aqua, "#00FFFF"}, {:brown, "#A52A2A"}, {:green, "#008000"}, {:navy, "#000080"}, {:olive, "#808000"}, {:pink, "#FFC0CB"}, {:purple, "#800080"}, {:red, "#FF0000"}, {:yellow, "#FFFF00"}]
        Home4.chooseColor(colors)
        IO.puts"Color not found in keyword list. Choosing loop ended"
    end


    def chooseColor(keywordlist) do
        input = IO.gets("Enter color name or HTML code: ") |> String.trim()
        if String.starts_with?(input, "#") do
            x = keywordlist |> Enum.find( fn {_key, val} -> val == input end)
            if x != nil do
                IO.puts(elem(x,0))
                Home4.chooseColor(keywordlist)
            end                                                
        else
            x = keywordlist |> Keyword.get(String.to_atom(input)) 
            if x != nil do
                IO.puts(x)
                Home4.chooseColor(keywordlist)
            end
        end      
    end


    def taskTwo() do
        books = %{"1111" => "Book One", "2222" => "Book Two", "3333" => "Book Three", "4444" => "Book Four", "5555" => "Book Five"}
        IO.puts "Welcome to N2942 Library : Options"
        IO.puts "1 : List all books"
        IO.puts "2 : Search books by ISBN"
        IO.puts "3 : Add new book"
        IO.puts "4 : Remove book by ISBN"
        IO.puts "5 : Exit library"
        Home4.readCommands(books)
        IO.puts "Exiting library"
    end

    def readCommands(books) do
        number = IO.gets ("Enter option nr : ") 
        corrected = String.trim(number)
        cond do
            corrected == "1" -> Home4.listBooks(books)
            corrected == "2" -> Home4.searchBooks(books)
            corrected == "3" -> Home4.addBook(books)
            corrected == "4" -> Home4.removeBook(books)
            corrected == "5" -> Home4.exitLibrary()
            true -> readCommands(books)
        end
    end

    def listBooks(books) do
        IO.puts "Listing books ..."
        IO.puts "[ISBN] [TITLE]"
        books
        |> Enum.each( fn {key, val} -> IO.puts"#{key}   #{val}" end)
        readCommands(books)
    end

    def searchBooks(books) do
        isbn = IO.gets ("Searching by ISBN -> Enter ISBN number : ")
        searchIsbn = String.trim(isbn)
        x = books |> Map.get(searchIsbn)
        if (x != nil) do
            IO.puts "Found book -> " 
            IO.puts "ISBN -> #{isbn}TITLE -> #{x}"
        else
            IO.puts "Book not found"
        end
        readCommands(books)
    end

    def addBook(books) do
        IO.puts "Addind new book ..."
        newIsbn = IO.gets("Enter ISBN number : ")
        correctedIsbn = String.trim(newIsbn)
        newTitle = IO.gets("Enter book title : ")
        correctedTitle = String.trim(newTitle)
        updatedBooks = Map.put_new(books, correctedIsbn, correctedTitle)
        IO.puts "New book added!"
        readCommands(updatedBooks)
    end

    def removeBook(books) do
        IO.puts "Removing book ..."
        removeBook = IO.gets ("Enter ISBN to remove : ")
        correctedRemove = String.trim(removeBook)
        updatedBooks = Map.delete(books, correctedRemove)
        IO.puts"Remove complete!"
        readCommands(updatedBooks)
    end

    def exitLibrary() do
        IO.puts "Thank you for visiting!"
    end


end
Home4.taskOne()
Home4.taskTwo()

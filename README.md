# Functional Programming with Elixir

This repository contains some of my homework assignments for University course Functional Programming (with Elixir)

Assignments:

**books.exs**

Part 1

* Declare a keyword list that contains name of the color and color html value.
* See html color values here
* Add at least 10 colors to the keyword list
* Create a loop that asks user the color name or color html value.
* If entered text begins with '#', print the corresponding color name.
* Otherwise print the corresponding html color value.
* If neither is found in keyword list, exit the loop.

Part 2

* Declare a map that contains book ISBN as a key and book name as a value.
* Add at least 5 books into the map
* Create a loop that reads commands from the user:
* list lists all books in the map.
* search ISBN searches a book with specified ISBN and prints book info.
* add ISBN,NAME adds new book into the map.
* remove ISBN removes book with ISBN if found on map.
* quit exits the loop.

**simple_calculator**

* Create a source file math.ex and declare a module Math.
* Declare functions Math.add, Math.sub, Math.mul, and Math.div, each taking two parameters.
* Declare a private function Math.info which prints info from above public functions
* The Math.add calls Math.info which prints "Adding x and y" (x and y the actual parameters to Math.add)
* Use Math.info similarly from sub, mul and div functions.
* Create a source file calculator.ex and declare module Calculator.
* Declare a function Calculator.calc that takes a string parameter.
* From string parameter, parse a number, operator (+,-,*,/) and second number, for example 123+456
* Call the corresponding Math function based on parsed operator and two numbers.
* Create a script that has a loop that asks a string from user
* The text user enters is passed to Calculator.calc function
* Exit the loop if user enters text that does not parse correctly in Calculator.calc

**employee.exs**

* Define a module and struct Employee, and add struct fields: first name, last name, id number, salary, job {:none, :coder, :designer, :manager, :ceo}
* id number has a default value that is previous id + 1
* salary has a default value 0
* job has a default value of :none
* Implement functions Employee.promote and Employee.demote which updates the job accordingly.
* job :none salary is set to 0
* each job above :none gets 2000 more salary than previous
* Test your Employee module and its promote and demote functions.

**blackjack.exs**

* Create two processes of player and dealer.
* Use inter-process communication to handle all aspects of the game between player and dealer.
* No need to implement betting, each game results of win or loss for the player.
* No need to implement hidden cards, assume that player and dealer cards are visible.
* Allow option to play again.

**genserver.exs**

* Use a GenServer to produce general purpose periodical timer
* An interface to start periodical timer with period in milliseconds and function to be called when timer triggers.
* Option to cancel the timer per return value (:ok or :cancel) of the passed function.
* Option to cancel the timer via public interface.


